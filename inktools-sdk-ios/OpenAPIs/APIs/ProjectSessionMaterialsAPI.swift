//
// ProjectSessionMaterialsAPI.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation



public class ProjectSessionMaterialsAPI: InkToolsAPI {

    public static let sharedInstance = ProjectSessionMaterialsAPI()

    /**
     Create a Project Session Material
     - POST /v1/project_session_materials
     - API Key:
       - type: apiKey Authorization 
       - name: Bearer
     - parameter params: (body)  
     - returns: APIResult<ProjectSessionMaterial> 
     */
    public func create(params: CreateProjectSessionMaterial, completion: @escaping(APIResult<ProjectSessionMaterial>) -> Void) {
        let path = "/v1/project_session_materials"
        let URLString = baseURL + path
        var parameters: Data? = nil
        do {
            let encoder = JSONEncoder()
            encoder.dateEncodingStrategy = .iso8601
            parameters = try encoder.encode(params)
        }
        catch {
            return completion(.error("502", "JSON Serialization error"))
        }

        let urlComponents = URLComponents(string: URLString)

        var request = requestWithDefaultHeaders(url: urlComponents!.url!)
        request.httpMethod = "POST"
        request.httpBody = parameters
        let task = defaultSession.dataTask(with: request) { data, response, error in
            completion(self.parseServerResponse(data: data, response: response, error: error))
        }
        task.resume()
    }


    /**
     Delete a Project Session Material
     - DELETE /v1/project_session_materials/{id}
     - API Key:
       - type: apiKey Authorization 
       - name: Bearer
     - parameter id: (path)  
     - returns: APIResult<DeletedResponse> 
     */
    public func delete(id: String, completion: @escaping(APIResult<DeletedResponse>) -> Void) {
        var path = "/v1/project_session_materials/{id}"
        let idPreEscape = "\(APIHelper.mapValueToPathItem(id))"
        let idPostEscape = idPreEscape.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) ?? ""
        path = path.replacingOccurrences(of: "{id}", with: idPostEscape, options: .literal, range: nil)
        let URLString = baseURL + path
        let parameters: [String:Any]? = nil
        
        let urlComponents = URLComponents(string: URLString)

        var request = requestWithDefaultHeaders(url: urlComponents!.url!)
        request.httpMethod = "DELETE"
        let task = defaultSession.dataTask(with: request) { data, response, error in
            completion(self.parseServerResponse(data: data, response: response, error: error))
        }
        task.resume()
    }

}
