//
// SuppliersAPI.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation



public class SuppliersAPI: InkToolsAPI {

    public static let sharedInstance = SuppliersAPI()

    /**
     Retrieves a list of Suppliers
     - GET /v1/suppliers
     - API Key:
       - type: apiKey Authorization 
       - name: Bearer
     - returns: APIResult<[Supplier]> 
     */
    public func index(completion: @escaping(APIResult<[Supplier]>) -> Void) {
        let path = "/v1/suppliers"
        let URLString = baseURL + path
        let parameters: [String:Any]? = nil
        
        let urlComponents = URLComponents(string: URLString)

        var request = requestWithDefaultHeaders(url: urlComponents!.url!)
        request.httpMethod = "GET"
        let task = defaultSession.dataTask(with: request) { data, response, error in
            completion(self.parseServerResponse(data: data, response: response, error: error))
        }
        task.resume()
    }

}
