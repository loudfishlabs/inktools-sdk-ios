//
// AccountActivationsAPI.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation



public class AccountActivationsAPI: InkToolsAPI {

    public static let sharedInstance = AccountActivationsAPI()

    /**
     Activates an Account
     - POST /v1/account_activations
     - API Key:
       - type: apiKey Authorization 
       - name: Bearer
     - parameter params: (body)  
     - returns: APIResult<UserSession> 
     */
    public func create(params: CreateAccountActivation, completion: @escaping(APIResult<UserSession>) -> Void) {
        let path = "/v1/account_activations"
        let URLString = baseURL + path
        var parameters: Data? = nil
        do {
            let encoder = JSONEncoder()
            encoder.dateEncodingStrategy = .iso8601
            parameters = try encoder.encode(params)
        }
        catch {
            return completion(.error("502", "JSON Serialization error"))
        }

        let urlComponents = URLComponents(string: URLString)

        var request = requestWithDefaultHeaders(url: urlComponents!.url!)
        request.httpMethod = "POST"
        request.httpBody = parameters
        let task = defaultSession.dataTask(with: request) { data, response, error in
            completion(self.parseServerResponse(data: data, response: response, error: error))
        }
        task.resume()
    }

}
