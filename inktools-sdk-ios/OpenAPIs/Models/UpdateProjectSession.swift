//
// UpdateProjectSession.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation


public struct UpdateProjectSession: Codable {

    public var projectSession: UpdateProjectSessionProjectSession

    public init(projectSession: UpdateProjectSessionProjectSession) {
        self.projectSession = projectSession
    }

    public enum CodingKeys: String, CodingKey { 
        case projectSession = "project_session"
    }


}
