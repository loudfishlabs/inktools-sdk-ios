//
// CreateProjectReferenceImageProjectReferenceImage.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation


public struct CreateProjectReferenceImageProjectReferenceImage: Codable {

    public var projectId: Int
    public var document: Base64Document?

    public init(projectId: Int, document: Base64Document?) {
        self.projectId = projectId
        self.document = document
    }

    public enum CodingKeys: String, CodingKey { 
        case projectId = "project_id"
        case document
    }


}
