//
// ConsentFormVersion.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation


public struct ConsentFormVersion: Codable {

    public var id: Int
    public var consentFormId: Int
    public var version: String
    public var documentUrl: String

    public init(id: Int, consentFormId: Int, version: String, documentUrl: String) {
        self.id = id
        self.consentFormId = consentFormId
        self.version = version
        self.documentUrl = documentUrl
    }

    public enum CodingKeys: String, CodingKey { 
        case id
        case consentFormId = "consent_form_id"
        case version
        case documentUrl = "document_url"
    }


}
