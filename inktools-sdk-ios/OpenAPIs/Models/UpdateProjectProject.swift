//
// UpdateProjectProject.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation


public struct UpdateProjectProject: Codable {

    public var name: String?
    public var startDate: Date?
    public var clientId: Int?
    public var studioId: Int?
    public var specialInstructions: String?
    public var totalPrice: Double?
    public var status: String?

    public init(name: String?, startDate: Date?, clientId: Int?, studioId: Int?, specialInstructions: String?, totalPrice: Double?, status: String?) {
        self.name = name
        self.startDate = startDate
        self.clientId = clientId
        self.studioId = studioId
        self.specialInstructions = specialInstructions
        self.totalPrice = totalPrice
        self.status = status
    }

    public enum CodingKeys: String, CodingKey { 
        case name
        case startDate = "start_date"
        case clientId = "client_id"
        case studioId = "studio_id"
        case specialInstructions = "special_instructions"
        case totalPrice = "total_price"
        case status
    }


}
