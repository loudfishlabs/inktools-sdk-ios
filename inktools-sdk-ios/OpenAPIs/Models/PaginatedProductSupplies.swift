//
// PaginatedProductSupplies.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation


public struct PaginatedProductSupplies: Codable {

    public var data: [ProductSupply]
    public var paginationMetadata: PaginationMetadata

    public init(data: [ProductSupply], paginationMetadata: PaginationMetadata) {
        self.data = data
        self.paginationMetadata = paginationMetadata
    }

    public enum CodingKeys: String, CodingKey { 
        case data
        case paginationMetadata = "pagination_metadata"
    }


}
