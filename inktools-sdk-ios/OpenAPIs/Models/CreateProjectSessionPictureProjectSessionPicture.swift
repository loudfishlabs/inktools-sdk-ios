//
// CreateProjectSessionPictureProjectSessionPicture.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation


public struct CreateProjectSessionPictureProjectSessionPicture: Codable {

    public var projectSessionId: Int
    public var document: Base64Document

    public init(projectSessionId: Int, document: Base64Document) {
        self.projectSessionId = projectSessionId
        self.document = document
    }

    public enum CodingKeys: String, CodingKey { 
        case projectSessionId = "project_session_id"
        case document
    }


}
