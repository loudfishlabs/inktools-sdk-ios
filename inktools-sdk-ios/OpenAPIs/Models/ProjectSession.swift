//
// ProjectSession.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation


public struct ProjectSession: Codable {

    public var id: Int
    public var projectId: Int
    public var sessionNumber: Int
    public var notes: String
    public var endDate: Date?
    public var status: String
    public var pictures: [ProjectSessionPicture]
    public var acceptedConsentFormVersions: [AcceptedConsentFormVersion]
    public var usedMaterials: [ProjectSessionMaterial]
    public var payment: ProjectPayment?
    public var appointment: Appointment?

    public init(id: Int, projectId: Int, sessionNumber: Int, notes: String, endDate: Date?, status: String, pictures: [ProjectSessionPicture], acceptedConsentFormVersions: [AcceptedConsentFormVersion], usedMaterials: [ProjectSessionMaterial], payment: ProjectPayment?, appointment: Appointment?) {
        self.id = id
        self.projectId = projectId
        self.sessionNumber = sessionNumber
        self.notes = notes
        self.endDate = endDate
        self.status = status
        self.pictures = pictures
        self.acceptedConsentFormVersions = acceptedConsentFormVersions
        self.usedMaterials = usedMaterials
        self.payment = payment
        self.appointment = appointment
    }

    public enum CodingKeys: String, CodingKey { 
        case id
        case projectId = "project_id"
        case sessionNumber = "session_number"
        case notes
        case endDate = "end_date"
        case status
        case pictures
        case acceptedConsentFormVersions = "accepted_consent_form_versions"
        case usedMaterials = "used_materials"
        case payment
        case appointment
    }


}
