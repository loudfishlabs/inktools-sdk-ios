//
// UpdateProjectSessionProjectSession.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation


public struct UpdateProjectSessionProjectSession: Codable {

    public var notes: String?
    public var appointmentAttributes: UpdateProjectSessionProjectSessionAppointmentAttributes?

    public init(notes: String?, appointmentAttributes: UpdateProjectSessionProjectSessionAppointmentAttributes?) {
        self.notes = notes
        self.appointmentAttributes = appointmentAttributes
    }

    public enum CodingKeys: String, CodingKey { 
        case notes
        case appointmentAttributes = "appointment_attributes"
    }


}
