//
// UpdateClientIdentityDocument.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation


public struct UpdateClientIdentityDocument: Codable {

    public var clientIdentityDocument: UpdateClientIdentityDocumentClientIdentityDocument

    public init(clientIdentityDocument: UpdateClientIdentityDocumentClientIdentityDocument) {
        self.clientIdentityDocument = clientIdentityDocument
    }

    public enum CodingKeys: String, CodingKey { 
        case clientIdentityDocument = "client_identity_document"
    }


}
