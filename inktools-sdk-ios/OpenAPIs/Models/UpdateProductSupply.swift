//
// UpdateProductSupply.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation


public struct UpdateProductSupply: Codable {

    public var productSupply: UpdateProductSupplyProductSupply

    public init(productSupply: UpdateProductSupplyProductSupply) {
        self.productSupply = productSupply
    }

    public enum CodingKeys: String, CodingKey { 
        case productSupply = "product_supply"
    }


}
