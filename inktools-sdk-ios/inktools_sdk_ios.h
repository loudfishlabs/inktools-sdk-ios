//
//  inktools_sdk_ios.h
//  inktools-sdk-ios
//
//  Created by Armeiv on 02/11/2020.
//  Copyright © 2020 Loudfish Labs. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for inktools_sdk_ios.
FOUNDATION_EXPORT double inktools_sdk_iosVersionNumber;

//! Project version string for inktools_sdk_ios.
FOUNDATION_EXPORT const unsigned char inktools_sdk_iosVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <inktools_sdk_ios/PublicHeader.h>


