How to generate classes from the openapi spec:

1. Download and install Java SDK
2. Download and unzip the latest release of https://github.com/OpenAPITools/openapi-generator
3. Copy the folder LFLSwift4 to openapi-generator-master/out/generators

4. Install the generator with: 
```
cd out/generators/LFLSwift4; mvn package; cd ../../../; java -cp out/generators/LFLSwift4/target/LFLSwift4-openapi-generator-1.0.0.jar:modules/openapi-generator-cli/target/openapi-generator-cli.jar org.openapitools.codegen.OpenAPIGenerator
```
5. Run the generator with (replace -i and -o routes accordingly):
```
java -cp out/generators/LFLSwift4/target/LFLSwift4-openapi-generator-1.0.0.jar:modules/openapi-generator-cli/target/openapi-generator-cli.jar \
  org.openapitools.codegen.OpenAPIGenerator generate -g lflswift4 \
  -i inktools-spec.yaml \
  -o ~/Desktop/inktoolsClient \
  --additional-properties="projectName=InkTools"
```
6. Replace the OpenAPIs folder in the project with the new one
7. Test that it builds with Product > Build
8. Test that it builds with carthage: 
```
carthage build --no-skip-current
```
9. Push the new version :)